<?php
$page_owner = elgg_get_page_owner_entity();

$options = array(
	'type' => 'object',
	'subtype' => 'livestream',
	'container_guid' => elgg_get_page_owner_guid(),
	'limit' => 6,
	'full_view' => false,
	'pagination' => false,
);


$content = elgg_list_entities($options);
$new_link = elgg_view('output/url', array(
	'href' => $CONFIG->wwwroot.'livestream/'.$page_owner->username.'/new/',
	'text' => elgg_echo('livestream:new_item'),
));
$viewall_link = elgg_view('output/url', array(
	'href' => $CONFIG->wwwroot . "livestream/" . $page_owner->username,
	'text' => elgg_echo('livestream:viewall'),
));

echo elgg_view('groups/profile/module', array(
	'title' => elgg_echo('livestream:group'),
	'content' => $content,
    'add_link' => $new_link,
    'all_link' => $viewall_link
));


?>
