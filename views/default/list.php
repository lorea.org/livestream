<?php
$page_owner = page_owner_entity();
$offset = get_input('offset', 0);

$options = Array(
    'type' => 'object',
    'subtype' => 'livestream',
	'container_guids' => Array($page_owner->guid),
	'offset' => $offset,
	'limit' => 4
);

echo elgg_list_entities($options);

?>