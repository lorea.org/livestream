<?php
	/**
	 * Elgg livestream plugin
	 * 
	 * @package LiveStream
	 */
$streamid = $vars['streamid'];
$item = livestream_get($streamid);

?>
<div class="contentWrapper">
<form action="<?php echo $vars['url']; ?>action/livestream/delete" enctype="multipart/form-data" method="post">
<?php echo elgg_view('input/securitytoken'); ?>
<p>
	<label>
		<?php echo elgg_echo("livestream:delete:ask"); ?><br />
	</label>
</p>


<?php
	if ($item->guid){
		echo elgg_view(
			'input/hidden',
			array(
				'internalname' => 'stream_id',
				'options_values' => $options, 
				'value' => $item->guid
			)
		);
	}
?>

<p><input type="submit" value="<?php echo elgg_echo('livestream:delete'); ?>" /></p>
 
</form>
</div>
