<?php
	/**
	 * Elgg livestream plugin
	 * 
	 * @package LiveStream
	 * @todo JS validation
	 */

	$form_body = "";
	$form_body.= elgg_echo("livestream:title")."<br />".elgg_view('input/text', array('internalname' => 'title', 'value' => ''))."<br />";
	$form_body.= elgg_echo("livestream:type")."<br />".elgg_view('input/radio', array('internalname' => 'mediatype', 'value' => '', 'options' => livestream_gettypes()))."<br />";
	$form_body.= elgg_echo("livestream:src").elgg_view('input/text', array('internalname' => 'src', 'value' => ''))."<br />";
	$form_body.= elgg_echo("livestream:width").elgg_view('input/text', array('internalname' => 'width', 'value' => '80'))."<br />";
	$form_body.= elgg_echo("livestream:height").elgg_view('input/text', array('internalname' => 'height', 'value' => '50'))."<br />";
	$form_body.= elgg_view(
		'input/hidden',
		array(
			'internalname' => 'container_guid',
			'options_values' => $options, 
			'value' => $vars['entity']->container_id?(int)$vars['entity']->container_id : page_owner()
		)
	);	
	$form_body.= "<input type='submit' value='".elgg_echo("livestream:save")."'>";
	echo elgg_view('input/form', array('body' => $form_body, 'action' => $vars['url']."action/livestream/new"));
	
	//$object = new ElggObject();
?>
