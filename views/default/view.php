<?php
	/**
	 * Elgg livestream plugin
	 * 
	 * @package LiveStream
	 */
$streamid = $vars['streamid'];
$item = livestream_get($streamid);

if($item){
	$src = $item->src;
	/* the following html code raises this funny notice
	 * 
	 * NOTICE: SELECT e.* from elgg_users_entity u join elgg_entities e on e.guid=u.guid where 
	 * 
	 * u.username='com.fluendo.player.Cortado.class' 
	 * 
	 * and ( (e.access_id = -2\n\t\t\tAND e.owner_guid 
	 * IN (\n\t\t\t\tSELECT guid_one FROM 
	 * elgg_entity_relationships\n\t\t\t\tWHERE relationship='friend' AND guid_two=6\n\t\t\t)) 
	 * OR  (e.access_id IN (2,1,4,5,6)\n\t\t\tOR (e.owner_guid = 6)\n\t\t\tOR 
	 * (\n\t\t\t\te.access_id = 0\n\t\t\t\tAND e.owner_guid = 6\n\t\t\t)\n\t\t) 
	 * and e.enabled='yes')  results cached, referer: 
	 * http://localhost/lorea/elgg/livestream/alice
	 * 
	 * */
	
	$autoplay = ($item->autoplay) ? "autoplay" : "";
	
	$row = "
	<div>
	$item->title<br />
	<video $autoplay src='$src' controls width=320 height=240>
	<applet code='com.fluendo.player.Cortado.class' archive='cortado.jar' height='240' width='320'>
	<param name='url' value='".$item->src."'>
	<param name='local' value='false'>
	<param name='keepaspect' value='true'>
	<param name='".$item->mediatype."' value='true'>
	<param name='bufferSize' value='60'>
	</applet>
	</video>
	<br />
	</div>
	"; 
	
	echo elgg_echo($row);
}


?>
