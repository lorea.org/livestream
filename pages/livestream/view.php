<?php
require_once (dirname(dirname(dirname(dirname(dirname(__FILE__))))) . "/engine/start.php");

global $CONFIG;
$page_owner = page_owner_entity();
$stream_id = get_input('streamid', '');



if (livestream_user_has_access()) {
	add_submenu_item(elgg_echo('livestream:new_item'),	$CONFIG->wwwroot."livestream/".$page_owner->username."/new/");
	add_submenu_item(elgg_echo('livestream:delete'),	$CONFIG->wwwroot."livestream/".$page_owner->username."/delete/".$stream_id);
}



//render
$body = elgg_view('view', Array('streamid' => get_input('streamid', '')));
$layout_canvas = "two_column_left_sidebar";
$layout_view = elgg_view_layout($layout_canvas, '', $body);

page_draw($title, $layout_view);
?>
