<?php
	/**
	 * Elgg livestream plugin
	 * 
	 * @package LiveStream
	 */
	require_once "{$CONFIG->pluginspath}livestream/libraries.php";
	

	function livestream_init()
	{
		global $CONFIG;
		
		// require libraries
		require_once "{$CONFIG->pluginspath}livestream/libraries.php";
		
		register_page_handler('livestream','livestream_page_handler');

		//enable for groups
		add_group_tool_option('groups',elgg_echo('livestream:enable'),true);
		
		//put in group hp
		elgg_extend_view('groups/right_column', 'livestream/groupprofile_livestream');
		
		
		if (isloggedin())
		{
			add_menu(elgg_echo('livestream:livestream'), $CONFIG->wwwroot . "livestream/" . $page_owner->username);
		}

		register_elgg_event_handler('pagesetup','system','livestream_pagesetup');
		//actions
		register_action("livestream/new", false, $CONFIG->pluginspath . "livestream/actions/new.php");
		register_action("livestream/delete", false, $CONFIG->pluginspath . "livestream/actions/delete.php");
	

	}

	function livestream_pagesetup(){
		global $CONFIG;
		
        	//Link to group items if comes form group environment
        	$page_owner = page_owner_entity();

	    	if ($page_owner instanceof ElggGroup && (get_context() == 'groups' || get_context() == 'group_profile')) {
 		           add_submenu_item(elgg_echo("livestream:livestream"), $CONFIG->url ."livestream/". $page_owner->username );
	    	}
    	
		//tools menu item
		$logged_user = get_loggedin_user();
		if($logged_user){
			add_menu(elgg_echo('livestream:livestream'), $CONFIG->wwwroot . "livestream/" . $logged_user->username);
		}
	}
	
	function livestream_page_handler($page)
	{
		global $_CONFIG;
      
		if (isset($page[0]) && $page[0]) {
			set_input('username',$page[0]);
		}
		
		if (isset($page[1]) && $page[1]) {
	    	switch($page[1]){
        		case 'new':
					include(dirname(__FILE__) . "/pages/livestream/new.php");
        		break; 
        		case 'view':
					set_input('username', $page[0]);
					set_input('streamid', $page[2]);
        			
					include(dirname(__FILE__) . "/pages/livestream/view.php");
        		break; 
        		case 'delete':
					set_input('username', $page[0]);
					set_input('streamid', $page[2]);
        			
					include(dirname(__FILE__) . "/pages/livestream/delete.php");
        		break; 
        		
				default:
					include(dirname(__FILE__) . "/pages/livestream/list.php");
				break;
			}
		}else{
			include(dirname(__FILE__) . "/pages/livestream/list.php");
		}
	
		return true;
	}
	
	register_elgg_event_handler('init','system','livestream_init');
	
?>
