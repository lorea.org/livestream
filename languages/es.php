<?php

// @translated by psy (https://lorea.cc)

$labels = array( 
	'livestream:new_item'  =>  "Nuevo stream", 
	'livestream:src'  =>  "URL Icecast", 
	'livestream:url:help'  =>  "Introduce una url de stream icecast.", 
	'livestream:width'  =>  "W",
	'livestream:height'  =>  "H",
	'livestream:title'  =>  "Título",
	'livestream:save'  =>  "Grabar",
	'livestream:back'  =>  "Atrás",
	'livestream:livestream'  =>  "Stream de video",
	'livestream:enable'  =>  "activar Livestream",
	'livestream:permission_denied' => 'operacion no permitida',
	'livestream:internal_error' => 'Error interno',
	'livestream:error:delete' => 'Error borrando el stream',
	'livestream:success:delete' => 'Stream borrado',
	'livestream:delete' => 'Borrar',
	'livestream:delete:ask' => 'Seguro?',
	'livestream:viewall' => 'Ver todos'
	
); 

add_translation('es', $labels); 

?>
