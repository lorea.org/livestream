<?php

// @translated by psy (https://lorea.cc)

$labels = array( 
	'livestream:new_item'  =>  "New stream", 
	'livestream:title'  =>  "Title", 
	'livestream:uri:help'  =>  "Introduce an icecast stream url.", 
	'livestream:src'  =>  "URL Icecast", 
	'livestream:width'  =>  "W",
	'livestream:height'  =>  "H",
	'livestream:save'  =>  "Save",
	'livestream:back'  =>  "Back",
	'livestream:livestream'  =>  "Livestream",
	'livestream:enable'  =>  "enable Livestream",
	'livestream:permission_denied' => 'operation not permitted',
	'livestream:internal_error' => 'Internal error',
	'livestream:error:delete' => 'Error while deleting a stream',
	'livestream:success:delete' => 'Stream deleted successfully',
	'livestream:delete' => 'Delete',
	'livestream:delete:ask' => 'Are you sure?',
	'livestream:viewall' => 'View all',
	'livestream:untitled' => 'Untitled',
	'livestream:type' => 'Type of stream',
	'livestream:group' => 'Group livestream',
	'livestream:error:notfound' => 'Stream not found'
	
); 

add_translation('en', $labels); 

?>
