<?php
function livestream_addnew($vals = Array()){
	$object = new ElggObject();
	$object->subtype = "livestream";
	$object->access_id = 2;
	
	$object->save();
	
	$object->title     = $vals['title'];
	$object->src       = $vals['src'];
	$object->width     = $vals['width'];
	$object->height    = $vals['height'];
	$object->mediatype = $vals['mediatype'];
	
	//needs to be implemented - depending to it needs to put 'autoplay' as string in the player 
	//template row, by now let's default to false
	$object->autoplay = false;
	
	//ownership & container
	$object->owner_guid = ($vals['owner_guid']) ? $vals['owner_guid'] : $object->owner_guid;
	$object->container_guid = ($vals['container_guid']) ? $vals['container_guid'] : $object->owner_guid;
	
	//check permissions
	
	/**
	 * @todo verify that works with admin users
	 * if no add elgg_is_admin_user(get_loggedin_userid())
	 */
	if(!$object->canEdit()){
		elgg_echo('livestream:permission_denied');
		$ret = false;
	}else{
		$object->save();
		$ret = $object;
	}
	
	return $ret;
	
}

function livestream_stream_setdefaults(&$stream_object){
	if(!$stream_object->title)     $stream_object->title = elgg_echo("livestream:untitled");
	if(!$stream_object->mediatype) $stream_object->mediatype = 'video';
}

function livestream_get($object_guid) {
	$object = get_entity($object_guid);
	if($object === false){
		register_error(elgg_echo('livestream:error:notfound'));
	}else{
		livestream_stream_setdefaults($object);
	}
	return $object;
}

function livestream_get_owner($object){
	$owner = get_user($object->getOwner());
	
	//if is not set, it's owned by root
	//@todo make username configurable
	if(!$owner) $owner = get_user_by_username('root');
	
	return $owner;
}

function livestream_delete($object_guid){
	$object = new ElggObject($object_guid);
	
	if(!$object_guid){
		register_error(elgg_echo('livestream:error:delete'));
		return false;
	}
	
	//check permissions
	/**
	 * @todo verify that works with admin users
	 * if no add elgg_is_admin_user(get_loggedin_userid())
	 */
	if(!$object->canEdit()){
		register_error(elgg_echo('livestream:error:delete'));
		elgg_echo('livestream:permission_denied');
		$ret = false;
	}else{
		$ret = $object->delete();
	}
	
	return $ret;
}

function livestream_gettypes(){
	return Array(
		'audio' => 'audio',
		'video' => 'video'
	);
}

function deleteall(){
	$page_owner = page_owner_entity();

	$data = elgg_get_entities(array(
    	'type' => 'object',
    	'subtype' => 'livestream',
		'container_guids' => Array($page_owner->guid)
	));
	
	foreach($data as $x => $item){
		if(!livestream_delete($item->guid)) {
			//do something
			return false;
		}
	}
	return true;
}

function livestream_get_container_name($object_guid){
	$myObject = livestream_get($object_guid);
	$container = livestream_get_container($myObject);
	return $container->username;
}

function livestream_get_container($myObject){
	if ($myObject->container_guid){
		$container = get_entity($myObject->container_guid);
	}else{
		$container = get_entity($myObject->owner_guid);
	}
	return $container;
}

function livestream_user_has_access(){
	return (isloggedin() && can_write_to_container(get_loggedin_userid(), $page_owner->guid, 'livestream'));
}
?>