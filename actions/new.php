<?php 
	require_once "{$CONFIG->pluginspath}livestream/libraries.php";

	$params = Array();
	$params['title']     = get_input('title', '');
	$params['mediatype'] = get_input('mediatype', '');
	$params['src']       = get_input('src', '');
	$params['width']     = get_input('width', '');
	$params['height']    = get_input('height', '');

	$params['owner_guid']     = page_owner();
	$params['container_guid'] = get_input('container_guid', '');
	
	$myObject = livestream_addnew($params);
	
	if($myObject){
		$container = livestream_get_container($myObject);
		forward('livestream/'.$container->username);
	}else{
		elgg_echo ("livestream:internal_error");
	}
	
?>
