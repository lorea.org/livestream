<?php 
	require_once "{$CONFIG->pluginspath}livestream/libraries.php";

	// get object guid from URL
	$object_guid = get_input('stream_id');
	$container_name = livestream_get_container_name($object_guid);
	
	if(livestream_delete($object_guid)){
		forward('livestream/'.$container_name);
	}else{
		elgg_echo ("livestream:internal_error");
		
	}
	
?>
